var itemList = [];
var itemPrice = [];

function openPopup(itemName, itemPrice) {
    var qtyPopup = document.getElementById("QuantityPopup");
    var qty = document.getElementById("quantity");
    qty.value = '';
    if (qtyPopup.style.display === "none") {
        qtyPopup.style.display = "block";
        var item = document.getElementById("itemName");
        var price = document.getElementById("itemPrice");
        item.value = itemName;
        price.value = itemPrice;
    } else {
        qtyPopup.style.display = "none";
    }
}

function btnAddToCart() {
    var qty = document.getElementById("quantity");
    if (isNaN(qty.value)) {
        alert("Please enter numeric value!");
    } else if (parseFloat(qty.value) <= 0) {
        alert("Please enter valid numer!");
    } else if (qty.value === '') {
        alert("Please enter numer!");
    } else {
        var item = document.getElementById("itemName");
        var price = document.getElementById("itemPrice");
        itemList.push(item.value + '-' + qty.value + '- $ ' + parseFloat(price.value) * parseFloat(qty.value));
        itemPrice.push(parseFloat(price.value) * parseFloat(qty.value));
        var qtyPopup = document.getElementById("QuantityPopup");
        qtyPopup.style.display = "none";
    }
}

function btnCancelCart() {
    var qtyPopup = document.getElementById("QuantityPopup");
    qtyPopup.style.display = "none";
}

function checktItem() {
    if (itemList.length > 0) {
		 var name = document.getElementById("name");
		 name.value = '';
        var namePopup = document.getElementById("NamePopup");
        namePopup.style.display = "block";
    } else {
        alert("Please purchase atleast 1 item!");
    }
}

function btnCheckout() {
    var name = document.getElementById("name");
    if (name.value === '') {
        alert("Please enter your name");
    } else {
        var reciptBox = document.getElementById("reciptBox");
        var mainBox = document.getElementById("mainbox");
        reciptBox.style.display = "block";
        mainBox.style.display = "none";

        var customerName = document.getElementById("customerName");
        customerName.innerHTML = 'Customer name: ' + name.value;

        var total = 0;
        for (let p = 0; p < itemPrice.length; p++) {
            total += parseFloat(itemPrice[p]);
        }
        var hst = (parseFloat(total) * 13) / 100;

        var temp = '';
        for (let i = 0; i < itemList.length; i++) {
            var itemDetails = itemList[i].split("-");
            temp += "<tr>";
            for (let j = 0; j < itemDetails.length; j++) {
                temp += "<td>";
                if (itemDetails.length)
                    temp += itemDetails[j];
                temp += "</td>";
            }
            temp += "</tr>";
        }
		var totalPrice = parseFloat(total) + parseFloat(hst.toFixed(2));
        temp += "<tr>";
        temp += "<td colspan='2'>HST @ 13%</td>";
        temp += "<td>";
        temp += "$ " + hst.toFixed(2);
        temp += "</td>";
        temp += "</tr>";
        temp += "<tr>";
        temp += "<td colspan='2'>Total</td>";
        temp += "<td>";
        temp += "$ " + totalPrice;
        temp += "</td>";
        temp += "</tr>";

        var tblItemList = document.getElementById("tblItemList");
        tblItemList.innerHTML = temp;
    }
}

function btnCancelCheckout() {
    var namePopup = document.getElementById("NamePopup");
    namePopup.style.display = "none";
}